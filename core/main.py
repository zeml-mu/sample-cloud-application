from flask import Flask
from flask import jsonify
import pika
import os


hostname = os.getenv("RABBITMQ_RABBITMQ_SVC_SERVICE_HOST", "localhost")
result = None

app = Flask(__name__)


@app.get('/')
def index():
    global result
    result = None
    result_dict = dict()
    result_dict["model"] = "send"
    connect = pika.BlockingConnection(pika.ConnectionParameters(host=hostname))
    channel = connect.channel()
    channel.exchange_declare("test", exchange_type="direct")
    q = channel.queue_declare(queue="").method.queue
    channel.queue_bind(exchange="test", queue=q)
    channel.basic_consume(queue=q, on_message_callback=on_response)
    channel.basic_publish(
        exchange="test",
        routing_key="q",
        properties=pika.BasicProperties(reply_to=q),
        body="ping"
    )
    while result is None:
        connect.process_data_events()
    result_dict["model_result"] = str(result)
    connect.close()
    return jsonify(result_dict)


def on_response(ch, method, props, body):
    global result
    result = body
    ch.basic_ack(method.delivery_tag)


if __name__ == '__main__':
    app.run("0.0.0.0", port=8000)