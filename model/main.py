import pika
import os


hostname = os.getenv("RABBITMQ_RABBITMQ_SVC_SERVICE_HOST", "localhost")


def rabbit_loop():
    connect = pika.BlockingConnection(pika.ConnectionParameters(host=hostname))
    channel = connect.channel()
    channel.exchange_declare("test", exchange_type="direct")
    channel.queue_declare(queue="q")
    channel.queue_bind(exchange="test", queue="q")
    channel.basic_consume(queue="q", on_message_callback=on_response)
    channel.start_consuming()


def on_response(ch, method, props, body):
    result = str(body) + " pong"
    ch.basic_ack(method.delivery_tag)
    connect = pika.BlockingConnection(pika.ConnectionParameters(host=hostname))
    channel = connect.channel()
    channel.exchange_declare("test", exchange_type="direct")
    channel.basic_publish(
        exchange="test",
        routing_key=props.reply_to,
        body=result
    )
    connect.close()


if __name__ == '__main__':
    print("It's work! Model!")
    rabbit_loop()
